---
layout: slide
title: Presentations&#58; Arquitectura de Hardware All slides
date:   2016-11-18 15:33:14 -0300
description: Article on markdown format.
categories: presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---

<p style='color:#214063'>
Auto play and all the slides of the project.
</p>
<!--more-->

<section data-autoslide="10000" data-background="https://gitlab.com/iush/hardware-architecture/raw/master/images/background.png" data-markdown data-separator="^----" data-separator-vertical="^====" >

# HARDWARE ARCHITECTURE

In engineering, hardware architecture refers to the identification of a system's physical components and their interrelationships.

![HARDWARE ARCHITECTURE]https://en.wikipedia.org/wiki/Hardware_architecture

![Google search](https://gitlab.com/iush/hardware-architecture/raw/master/images/GoogleSearchHardwareArch.png "Google searh: hardware architecture")

----

## Inteligent Classroom before a Inteligent City

We think that we need a clever Classroom before think on a clever City.

----

## A short range, ultra-low power consuming wireless technology.

Shares the “Bluetooth” name, but has different design goals in mind.

----

## Power Consumption

Years, not hours or days.

----

## Short range

About 50m

----

## Packet-based

- BLE is a “star” topology network

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/paket.png "Emparejamiento")

_
====

## The Stack

- Controller
- Host

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack.png "Emparejamiento")

_
====

### Logical Link Control and Adaptation Protocol (L2CAP)

- MTU
- Channel Management
- Connection parameter updates

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack1.png "Emparejamiento")

_
====

### Maximum Transmission Unit (MTU)

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/mtu.png "Emparejamiento")

_
====

### Security Manager Protocol (SMP)

- Pairing
- Authentification
- Bonding

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack2.png "Emparejamiento")

_
====

### Generic Access Profile (GAP)

- Device Discovery
- Link Establishment
- Link Management
- Link Termination
- Initiation of security features

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Stack3.png "Emparejamiento")

----

### Attribute Protocol (ATT)

- A handle (address)
- A type
- A set of permissions

_
====

### Central vs Peripheral

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/centravsperi.png "Emparejamiento")
centravsperi
_
====

### Generic Attribute Profile (GATT)

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Gatt.png "Emparejamiento")
_
====

### Intervals

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Emparejamiento.png "Emparejamiento")

_
====

### Connection-less
Devices do not need to maintain connections.

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/NetworkStates.png "Emparejamiento")

_
====

### Interval

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/ConInterval.png "Emparejamiento")

_
====

### Pairing

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/Link.png "Emparejamiento")

----

## Security
Devices pair, keys are distributed, and the connection is encrypted.

Encryption is AES-128.

----

## Two kinds of services
There are primary services and secondary services.

Services can contain other services. (Nested services)

_
====

## Profiles

Profiles define roles for devices to play.

_
====

## Profiles & Services

Profiles contain services.
Services can be contained by multiple profiles.

_
====

## Anatomy of a Peripheral

![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/anatomiPeri.png "Emparejamiento")

----

### Aplication

[Aplication
![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/app.png "Emparejamiento")](https://gitlab.com/oemunoz/force-ha)

_
====

### D3 Library

[D3](https://github.com/d3/d3-force)

_
====

### Bleno Library

[Bleno](https://github.com/sandeepmistry/bleno)

----

***BLUETOOTH LOW ENERGY (BLE)***

El Bluetooth Low Energy, o Bluetooth de baja energía es una tecnología
digital de radio inalámbrica que realiza operaciones entre dispositivos
pequeños autónomos.

![](https://gitlab.com/iush/blue-doc/raw/master/images/BLEPrototyping.png)

----

Es importante saber que los retos enfrentados por la tecnología
bluetooth radican en la descarga de la batería y la vinculación
constante y re-emparejamiento de los dispositivos conectados a través de
ella.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-services.png)

----

Del prototipo de bluetooth de baja energía o bluetooth low energy se
desprenden dos modelos desarrollados por el bluetooth special interest
group (SIG) tales como bluetooth Smart y bluetooth Smart Ready.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-versions.jpg)

----

**BLUETOOTH SMART**

Bluetooth Smart o Bluetooth inteligente es una tecnología basada en la
funcionalidad de bajo consumo de energía que permite emparejarse con
dispositivos Bluetooth Smart Ready y que se encargan de enviar
información. No necesariamente todo el tiempo deban enviar información
sino en cada momento en que el dispositivo receptor lo solicite.
Mientras no está enviando información el dispositivo puede entrar a un
modo de suspensión, habilitándose inmediatamente le sea solicitada
cierta información o se cumpla alguna regla previamente establecida.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-smart.jpg)

----

**BLUETOOTH SMART READY**

Bluetooth Smart Ready o Bluetooth inteligente listo es una tecnología
basada en la funcionalidad de bajo consumo de energía que permite
emparejarse con dispositivos Bluetooth, Bluetooth Smart o de su misma
esencia Bluetooth Smart Ready y que se encargan de enviar y recibir
información de dispositivos que no necesariamente se encuentran
monitoreados. Normalmente lo poseen los dispositivos primarios tales
como teléfonos inteligentes, computadores o tabletas y reciben o envían
información a dispositivos tales como auriculares, accesorios y demás.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-smart_1.jpg)

----

**¿Cuáles son las ventajas del Bluetooth de baja energía?**

-   Debido a que las conexiones se realizan mediante radiofrecuencias,
    se eliminan tanto cableado como conectores.

-   Ofrece la posibilidad de crear una red inalámbrica entre
    varios dispositivos.

-   Al ser tecnología BLE, no es un factor importante en el desgaste de
    la batería.

-   Ofrece una velocidad de 24MB/s

-   Es una tecnología de uso global, es decir, en cualquier parte del
    mundo puede ser usada.

----

**¿Cuáles son las desventajas del Bluetooth de baja energía?**

-   La tecnología Bluetooth no ofrece mayor seguridad en cuanto a su
    implementación, en algunas versiones puede ser vulnerable a perdida
    de información.

-   Su alcance es mínimo al momento de intercambiar información. (Máximo
    30 metros).

----

**¿ZIG BEE o BLUETOOTH LOW ENERGY?**

| **ASPECTO**  | **BLUETOOTH**  | **ZIGBEE** |
| :---------: | :-----: | :-------:
| **Distancia**       | En su última versión puede alcanzar hasta 30 metros de alcance.  |   En su última versión puede alcanzar hasta 150 metros de alcance. |
| **Seguridad**       | Ofrece una seguridad moderada.   |  Ofrece una seguridad buena avalada por su uso en la industria. |
| **Uso de batería**  | El uso de batería de la última versión es su valor agregado, pues es mínimo. | El uso de batería de la última versión es el más bajo entre los dispositivos que cumplen su misma función. |
| **Ancho de banda**  | Un ancho de banda regulado, con tendencia a la reducción.   | Un ancho de banda regulado, con tendencia a la reducción. |
| **Confiabilidad**   | Su confiabilidad es mínima cuando existen más de 10 dispositivos conectados por medio de ella. | Es de alta confiabilidad para usar máquina a máquina. |

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue_zigbee.jpg)

**Node.js**

Es un entorno de ejecución para JavaScript basado en el motor V8 de Google y actualmente cuenta con el ecosistema de librerias de código abierto mas grande del mundo llamada NPM. Es un entorno de ejecución orientado a eventos.

![NodeJs](https://gitlab.com/iush/blue-doc/raw/master/images/nodejs.jpg)

----

**Qué es y para que sirve Node.js?**

Node.js es un entorno de ejecución de JavaScript de lado del servidor, el cual permite la creación de aplicaciones de red altamente escalables. 

![NodeJs](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

----

**Qué es y para que sirve Node.js?**

Se basa en Programación Orientada a Eventos Asíncronos. Node.js cambia la manera de como conectarse a un servidor, ya que cada conexión dispara un evento especifico dentro del motor y se afirma que puede soportar decenas de miles de conexiones concurrentes sin bloqueos.

![NodeJs](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

----

**JavaScript en el servidor**

- Lenguaje de programación potente

- Posee un excelente modelo de eventos asincrónicos 

- Curva de aprendizaje reducida debido al previo conocimiento de muchos desarrolladores

![NodeJs](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

----

**Ventajas**

- Gran capacidad de tolerancia a la cantidad de peticiones simultaneas 

- Existencia de gran cantidad de módulos para casi todas las necesidades que se tengan gracias a NPM

- Menor costo de infraestructura

- Comunidad creciente dispuesta a resolver dudas y mostrar nuevas maneras de uso  

![NodeJs](https://gitlab.com/iush/blue-doc/raw/master/images/nodeacua.png)

----

**Electron.js**

Es un framework desarrollado por GitHub el cual permite el desarrollo de aplicaciones de escritorio usando Node.js y tecnologias para web como HTML, JavaScript y CSS.

![Electron](https://gitlab.com/iush/blue-doc/raw/master/images/electron.png)

----

## ** APLICABILIDAD DEL SENSOR BLUETOOTH **

Se realiza mediante dispositivos móviles con bluetooth en los cuales se incorporan aplicaciones u opciones que puedan localizar y rastrear la posición del usuario y actuar en base a esa información proporcionada por el lugar y el momento.

----

## Marketing de localización y proximidad por bluetooth

![RedBull](http://iush.gitlab.io//assets/images/RedBull.jpg)

----

## Ubicación de personas en un sitio determinado

![ClassRoom](http://iush.gitlab.io//assets/images/clase.png)

----

## Ubicación de elementos en un sitio determinado

![Enterprise](http://iush.gitlab.io//assets/images/empresa.png)

</section>
