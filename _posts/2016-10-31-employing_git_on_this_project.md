---
bg: "tools.jpg"
layout: post
title:  "Employing git on this project"
crawlertitle: "About GIT"
summary: "Git for the Classroom"
date:   2016-10-31 20:09:47 +0700
categories: posts
tags: ['front-end']
author: oems
---

Using GitLab/GitHub for this project.
[![railroad]({{ site.images }}/git.jpg)]({{ site.images }}/git.jpg)

----

# GitLab/GitHub

Web based service that utilizes the Git distributed version control system.
 - [IBM](https://ibm.github.io/)
 - [Microsoft](https://microsoft.github.io/)

[![railroad]({{ site.images }}/GitAll.png)]({{ site.images }}/GitAll.png)

----

# References + Further Reading
- [Use of GitHub as a Platform for Open Collaboration on Text Documents](http://www.opensym.org/os2015/proceedings-files/p503-longo.pdf)
- [LESSONS FOR THE CLASSROOM AND NEWSROOM](http://www.storybench.org/use-github-lessons-classroom-newsroom/)
- [Student Experiences Using GitHub](http://keg.cs.uvic.ca/pubs/feliciano-ICSE2016.pdf)
- [The Code-Centric Collaboration Perspective](http://www.opensym.org/os2015/proceedings-files/p503-longo.pdf)
- [The Emergence of GitHub as a Collaborative Platform for Education](http://alexeyza.com/pdf/cscw15.pdf)
- [The Use of GitHub in Computer Science and Software Engineering Courses](http://keg.cs.uvic.ca/theses/Feliciano_Joseph_MSc_2015.pdf)
- [Use of GitHub as a Platform for Open Collaboration on Text Documents](http://www.opensym.org/os2015/proceedings-files/p503-longo.pdf)
