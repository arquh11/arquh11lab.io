---
bg: "tools.jpg"
layout: post
title:  "Trabajo General Arquitectura de Hardware."
date:   2016-10-20 15:32:14 -0300
crawlertitle: "General Description"
summary: "High Level Deign"
categories: jekyll hwarch posts
tags: ['front-end']
excerpt_separator: <!--more-->
---
To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

Jekyll also offers powerful support for code snippets.


<!--more-->

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/hardware-architecture/raw/master/2016-10-20-hardware-architecture.markdown %}
