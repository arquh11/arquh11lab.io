---
layout: slide
title: Presentations&#58; Bluetooth Low Energy
date:   2016-10-31 15:33:15 -0300
description: Article on markdown format.
categories: posts presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---
<p style='color:#214063'>
Bluetooth Low Energy (BLE), Bluetooth de baja energía es una tecnología digital de radio inalámbrica que realiza operaciones entre dispositivos pequeños autónomos.
</p>
[![railroad]({{ site.images }}/blue-services.png)]({{ site.images }}/blue-services.png)
<!--more-->

<section data-background="https://gitlab.com/iush/hardware-architecture/raw/master/images/background.png" data-markdown data-separator="^----" data-separator-vertical="^====" >
<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/blue-doc/raw/master/2016-10-31-bluetooth-architecture.md %}
</section>
