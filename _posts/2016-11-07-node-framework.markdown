---
layout: slide
title: Presentations&#58; NodeJs framework.
date:   2016-11-08 15:33:15 -0300
description: Article on markdown format.
categories: posts presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---
<p style='color:#214063'>
Node.js es una plataforma construida encima del entorno de ejecución javascript de Chrome para fácilmente construir rápidas, escalables aplicaciones en red.
</p>
[![railroad]({{ site.images }}/nodeacua.png)]({{ site.images }}/nodeacua.png)
<!--more-->

<section data-background="https://gitlab.com/iush/hardware-architecture/raw/master/images/background.png" data-markdown data-separator="^----" data-separator-vertical="^====" >
<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/blue-doc/raw/master/2016-11-07-node-architecture.md %}
</section>
