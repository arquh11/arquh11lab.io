---
layout: slide
title: Presentations&#58; Arquitectura de Hardware
date:   2016-10-20 15:33:14 -0300
description: Article on markdown format.
categories: presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---
<p style='color:#214063'>
Arquitectura de hardware.
</p>
<!--more-->

<section data-background="https://gitlab.com/iush/hardware-architecture/raw/master/images/background.png" data-markdown data-separator="^----" data-separator-vertical="^====" >
<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/hardware-architecture/raw/master/2016-10-20-hardware-architecture.markdown %}
</section>
